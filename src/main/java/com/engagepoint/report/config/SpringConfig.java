package com.engagepoint.report.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.engagepoint")
public class SpringConfig {
}
