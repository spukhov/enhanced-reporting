package com.engagepoint.report.common;

/**
 * @author denis.bilyk
 */
public enum XmlParseElements {
    XML_EL_NAME("aud:name"),
    XML_EL_TIMESTAMP("aud:timestamp"),
    XML_EL_SOURCE_ID("aud:sourceId"),
    XML_EL_SOURCE_ADDR("aud:sourceAddress"),
    XML_EL_INFO("aud:info"),
    XML_EL_ATTRIBUTE("aud:attribute");

    private String value;
    XmlParseElements(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
