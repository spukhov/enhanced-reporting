package com.engagepoint.report.transform;

import com.engagepoint.report.core.DataParser;
import com.engagepoint.report.core.DtoParseException;
import com.engagepoint.report.core.EventObjectDto;
import com.engagepoint.report.dao.EventObjectDao;
import com.engagepoint.report.dao.HBaseDao;
import com.engagepoint.report.dao.SourceLogDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import java.io.IOException;

import static com.engagepoint.report.dao.HBaseDao.COLUMN_FAMILY_DATA;
import static com.engagepoint.report.dao.HBaseDao.QUALIFIER_EMPTY;

/**
 * @author denis.bilyk
 */
@Slf4j
public class SourceMapReducer {


    public static class SourceMapper extends TableMapper<ImmutableBytesWritable, Result> {

        @Override
        protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
            context.write(new ImmutableBytesWritable(value.getRow()), value);
        }

    }


    public static class SourceReducer extends TableReducer<ImmutableBytesWritable, Result, ImmutableBytesWritable> {

        @Override
        protected void reduce(ImmutableBytesWritable key, Iterable<Result> values, Context context) throws IOException, InterruptedException {
            for (Result value : values) {
                Text textValue = new Text(value.getValue(Bytes.toBytes(COLUMN_FAMILY_DATA), Bytes.toBytes(QUALIFIER_EMPTY)));
                try {
                    EventObjectDto dto = DataParser.parse(textValue.toString());
                    Put put = EventObjectDao.getPut(new String(key.get()), dto);
                    context.write(new ImmutableBytesWritable(HBaseDao.TABLE_NAME_DATA.getBytes()), put);
                    deleteProcessedRecord(key,context);
                } catch (DtoParseException e) {
                    Put put = SourceLogDao.getPut(new String(key.get()), textValue.toString());
                    context.write(new ImmutableBytesWritable(HBaseDao.TABLE_NAME_INVALID.getBytes()), put);
                    deleteProcessedRecord(key, context);
                }
            }
        }

        private void deleteProcessedRecord(ImmutableBytesWritable key, Context context) throws IOException, InterruptedException {
            Delete deleteProcessed = new Delete(Bytes.toBytes(new String(key.get())));
            context.write(new ImmutableBytesWritable(HBaseDao.TABLE_NAME_IN.getBytes()), deleteProcessed);
        }
    }
}
