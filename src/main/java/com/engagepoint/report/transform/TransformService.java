package com.engagepoint.report.transform;

import com.engagepoint.report.dao.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.*;
import org.apache.hadoop.mapreduce.Job;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class TransformService {


    public void transform() throws IOException, ClassNotFoundException, InterruptedException {
        Configuration config = HBaseConfiguration.create();

        config.set(TableInputFormat.INPUT_TABLE, HBaseDao.TABLE_NAME_IN);
        config.setStrings("io.serializations", config.get("io.serializations"),
                MutationSerialization.class.getName(), ResultSerialization.class.getName(),
                KeyValueSerialization.class.getName());

        Job job = Job.getInstance(config, "SourceParcer");
        job.setJarByClass(TransformService.class);

        job.setMapperClass(SourceMapReducer.SourceMapper.class);
        job.setReducerClass(SourceMapReducer.SourceReducer.class);

        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(Result.class);
        job.setInputFormatClass(TableInputFormat.class);
        job.setOutputFormatClass(MultiTableOutputFormat.class);

        boolean b = job.waitForCompletion(true);
        if (!b) {
            throw new IOException("OPACHA! Something went wrong.");
        }
    }

}
