package com.engagepoint.report.core;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author denis.bilyk
 */
@EqualsAndHashCode
@ToString
public class EventObjectDto implements Serializable {
    private static final long serialVersionUID = 23490872234234L;
    public static final String QUALIFIER_NAME = "name";
    public static final String QUALIFIER_TIMESTAMP = "timestamp";
    public static final String QUALIFIER_SOURCE_ID = "sourceId";
    public static final String QUALIFIER_SOURCE_ADDRESS = "sourceAddress";
    public static final String QUALIFIER_INFO = "info";
    private String name;
    private long timestamp;
    private String sourceId;
    private String sourceAddress;
    private String info;
    private Map<String, String> attributes;

    private EventObjectDto() {
        attributes = new HashMap<String, String>();
    }

    public static EventObjectDto create() {
        return new EventObjectDto();
    }

    public EventObjectDto withName(String name) {
        this.name = name;
        return this;
    }

    public EventObjectDto withTimestamp(long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public EventObjectDto withSourceId(String sourceId) {
        this.sourceId = sourceId;
        return this;
    }

    public EventObjectDto withSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
        return this;
    }

    public EventObjectDto withInfo(String info) {
        this.info = info;
        return this;
    }

    public EventObjectDto addAttribute(String key, String value) {
        attributes.put(key, value);
        return this;
    }

    public String getName() {
        return name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getSourceAddress() {
        return sourceAddress;
    }

    public String getInfo() {
        return info;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public String getAttribute(String key){
        if(attributes.containsKey(key)){
            return attributes.get(key);
        }
        return StringUtils.EMPTY;
    }

}
