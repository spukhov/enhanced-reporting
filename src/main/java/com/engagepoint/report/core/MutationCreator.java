package com.engagepoint.report.core;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * @author denis.bilyk
 */
public abstract class MutationCreator {

    public static Put getPut(String rowKey, PutAction putAction){
        Put put = new Put(Bytes.toBytes(String.valueOf(rowKey)));
        putAction.addPut(put);
        return put;
    }

    public static interface PutAction {
        void addPut(Put put);
    }
}
