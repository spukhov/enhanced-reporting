package com.engagepoint.report.core;

import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.Null;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;

/**
 * @author denis.bilyk
 */
@Slf4j
public final class DataParser {
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss.SSSXXX";

    private DataParser() {
    }

    @Null
    public static EventObjectDto parse(@Null InputStream is) throws DtoParseException{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser saxParser = factory.newSAXParser();
            SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
            SaxHandler handler = new SaxHandler(sdf);
            saxParser.parse(is, handler);
            return handler.getDto();
        } catch (Throwable err) {
            log.error("Can not parse xml string", err);
            throw new DtoParseException();
        }
    }

    @Null
    public static synchronized EventObjectDto parse(@Null String bareXmlString) throws DtoParseException{
        if (bareXmlString == null) return null;
        InputStream is = new ByteArrayInputStream(bareXmlString.getBytes(Charset.forName("UTF-8")));
        return parse(is);
    }
}
