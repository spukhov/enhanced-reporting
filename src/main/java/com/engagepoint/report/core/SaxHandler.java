package com.engagepoint.report.core;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Stack;

import static com.engagepoint.report.common.XmlParseElements.*;

/**
 * @author denis.bilyk
 */
public class SaxHandler extends DefaultHandler {
    private EventObjectDto dto;
    private Stack<String> elementStack = new Stack<String>();
    private SimpleDateFormat sdf;

    public SaxHandler(SimpleDateFormat sdf) {
        this.sdf = sdf;
        dto = EventObjectDto.create();

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (XML_EL_ATTRIBUTE.getValue().equals(qName)) {
            try {
                dto.addAttribute(attributes.getValue(0), attributes.getValue(1));
            } catch (IndexOutOfBoundsException e) {
                throw new SAXException(e);
            }
        }
        this.elementStack.push(qName);
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementStack.pop();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();
        if (value.length() == 0) return;

        if (XML_EL_NAME.getValue().equals(currentElement())) dto.withName(value);
        if (XML_EL_SOURCE_ID.getValue().equals(currentElement())) dto.withSourceId(value);
        if (XML_EL_SOURCE_ADDR.getValue().equals(currentElement())) dto.withSourceAddress(value);
        if (XML_EL_INFO.getValue().equals(currentElement())) dto.withInfo(value);
        if (XML_EL_TIMESTAMP.getValue().equals(currentElement())) {
            try {
                dto.withTimestamp(sdf.parse(value).getTime());
            } catch (ParseException e) {
                throw new SAXException(e);
            }
        }
    }

    private String currentElement() {
        return this.elementStack.peek();
    }

    public EventObjectDto getDto() {
        return dto;
    }
}
