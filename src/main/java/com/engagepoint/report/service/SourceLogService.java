package com.engagepoint.report.service;

import com.engagepoint.report.dao.EventObjectDao;
import com.engagepoint.report.dao.SourceLogDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class SourceLogService {

    @Autowired
    private EventObjectDao objectDao;

    @Autowired
    private SourceLogDao sourceDao;

    public void insert(String data) throws IOException {
        long rowKey = sourceDao.put(data);
        log.info("Saved string : " + data + "; key - " + rowKey);
    }

    public String get(String rowKey) throws IOException {
        log.info("getting data by key: " + rowKey);
        return objectDao.get(rowKey).toString();
    }

}
