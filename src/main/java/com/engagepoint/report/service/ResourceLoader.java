package com.engagepoint.report.service;


import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

/**
 * @author denis.bilyk
 */
@Slf4j
@Component
public class ResourceLoader {
    private static final String HBASE_CONF = "/hbase.properties";
    private HBaseAdmin conn;

    public synchronized HBaseAdmin getConnection() {
        if (conn == null) {
            conn = get();
        }
        return conn;
    }

    private HBaseAdmin get() {
        Properties properties = loadProps();
        Configuration conf = HBaseConfiguration.create();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            conf.set((String) entry.getKey(), (String) entry.getValue());
        }
        try {
            return new HBaseAdmin(conf);
        } catch (IOException e) {
            log.error("Smth went wrong.", e);
        }
        return null;
    }


    private static Properties loadProps() {
        InputStream resource = ResourceLoader.class.getResourceAsStream(HBASE_CONF);
        if (resource == null) {
            log.warn("Hbase properties can not be loaded");
            return new Properties();
        }
        Properties properties = new Properties();
        try {
            properties.load(resource);
        } catch (IOException e) {
            log.error("Hbase properties can not be loaded", e);
        }
        return properties;
    }

}
