package com.engagepoint.report.dao;

import com.engagepoint.report.core.EventObjectDto;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Map;
import java.util.NavigableMap;

import static com.engagepoint.report.core.EventObjectDto.*;
import static org.apache.hadoop.hbase.util.Bytes.toBytes;

/**
 * @author denis.bilyk
 */
@Repository
public class EventObjectDao extends HBaseDao {

    public static Put getPut(String rowKey, final EventObjectDto dto){
        return HBaseDao.getPut(rowKey, createPutAction(dto));
    }

    public EventObjectDto get(String rowKey) throws IOException {
        Result get = super.getGet(TABLE_NAME_DATA, rowKey);
        EventObjectDto dto = EventObjectDto.create();
        dto.withName(new String(get.getValue(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_NAME))))
                .withTimestamp(Long.parseLong(new String(get.getValue(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_TIMESTAMP)))))
                .withSourceId(new String(get.getValue(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_SOURCE_ID))))
                .withSourceAddress(new String(get.getValue(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_SOURCE_ADDRESS))))
                .withInfo(new String(get.getValue(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_INFO))));
        NavigableMap<byte[], byte[]> map = get.getFamilyMap(toBytes(COLUMN_FAMILY_ATTRIBUTES));
        for (Map.Entry<byte[], byte[]> entry : map.entrySet()) {
            dto.addAttribute(new String(entry.getKey()), new String(entry.getValue()));
        }
        return dto;
    }

    private static PutAction createPutAction(final EventObjectDto dto) {
        return new PutAction() {
            @Override
            public void addPut(Put put) {
                put.add(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_NAME), toBytes(dto.getName()));
                put.add(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_TIMESTAMP), toBytes(String.valueOf(dto.getTimestamp())));
                put.add(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_SOURCE_ID), toBytes(dto.getSourceId()));
                put.add(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_SOURCE_ADDRESS), toBytes(dto.getSourceAddress()));
                put.add(toBytes(COLUMN_FAMILY_DATA), toBytes(QUALIFIER_INFO), toBytes(dto.getInfo()));
                for (Map.Entry<String, String> entry : dto.getAttributes().entrySet()) {
                    put.add(toBytes(COLUMN_FAMILY_ATTRIBUTES), toBytes(entry.getKey()), toBytes(entry.getValue()));
                }
            }
        };
    }

}
