package com.engagepoint.report.dao;

import java.sql.*;

public class HiveHelper {
    private static String driverName = "org.apache.hive.jdbc.HiveDriver";

    public static void main(String[] args) throws SQLException {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        Connection con = DriverManager.getConnection("jdbc:hive2://quickstart.cloudera:10000/default", "hive", "");
        Statement stmt = con.createStatement();
        ResultSet res = stmt.executeQuery("show tables");
        // show tables
        if (res.next()) {
            System.out.println(res.getString(1));
        }
    }
}
