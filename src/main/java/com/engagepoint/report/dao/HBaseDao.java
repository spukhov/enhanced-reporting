package com.engagepoint.report.dao;


import com.engagepoint.report.service.ResourceLoader;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

@Slf4j
public abstract class HBaseDao  {
    public static final String TABLE_NAME_IN = "in";
    public static final String TABLE_NAME_DATA = "data";
    public static final String TABLE_NAME_INVALID = "invalid";
    public static final String COLUMN_FAMILY_DATA = "data";
    public static final String COLUMN_FAMILY_ATTRIBUTES = "attributes";
    public static final String QUALIFIER_EMPTY = "";
    public static final String QUALIFIER_DATA = "data";

    @Autowired
    ResourceLoader loader;


    static Put getPut(String rowKey, PutAction putAction){
        Put put = new Put(Bytes.toBytes(String.valueOf(rowKey)));
        putAction.addPut(put);
        return put;
    }

    long push(String tableName, PutAction putAction) throws IOException {
        HBaseAdmin hBaseAdmin = loader.getConnection();
        if (hBaseAdmin == null) return -1;
        long rowKey = System.currentTimeMillis();
        HTable table = new HTable(hBaseAdmin.getConfiguration(), tableName);
        Put put = new Put(Bytes.toBytes(String.valueOf(rowKey)));
        putAction.addPut(put);
        table.put(put);
        table.flushCommits();
        table.close();
        log.info("put - " + rowKey + "  value - " + put.toString());
        return rowKey;
    }

    Result getGet(String tableName, String rowKey) throws IOException {
        HBaseAdmin hBaseAdmin = loader.getConnection();
        HTable table = new HTable(hBaseAdmin.getConfiguration(), tableName);
        Get get = new Get(Bytes.toBytes(rowKey));
        return table.get(get);
    }

    public static interface PutAction {
        void addPut(Put put);
    }

}
