package com.engagepoint.report.dao;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Repository;

import java.io.IOException;

/**
 * @author denis.bilyk
 */
@Repository
@Slf4j
public class SourceLogDao extends HBaseDao {

    public static Put getPut(String rowKey, final String value){
        return HBaseDao.getPut(rowKey, new PutAction() {
            @Override
            public void addPut(Put put) {
                put.add(Bytes.toBytes(COLUMN_FAMILY_DATA), Bytes.toBytes(QUALIFIER_EMPTY), Bytes.toBytes(value));
            }
        });
    }

    public long put(final String value) throws IOException {
       return super.push(TABLE_NAME_IN, new PutAction() {
           @Override
           public void addPut(Put put) {
               put.add(Bytes.toBytes(COLUMN_FAMILY_DATA), Bytes.toBytes(QUALIFIER_DATA), Bytes.toBytes(value));
           }
       });
    }

    public String get(String rowKey) throws IOException {
        Result result = super.getGet(TABLE_NAME_IN, rowKey);
        byte[] value = result.getValue(Bytes.toBytes(COLUMN_FAMILY_DATA), Bytes.toBytes(QUALIFIER_DATA));
        String s = Bytes.toString(value);
        log.info(s);
        return s;
    }
}
