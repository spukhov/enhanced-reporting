package com.engagepoint.report.ws;

import com.engagepoint.report.service.SourceLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/log")
@Slf4j
public class LogWebService {

    @Autowired
    private SourceLogService sourceLogService;

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    public void saveLog(String logStr) throws IOException {
        sourceLogService.insert(logStr);
    }

    @GET
    public String getLog(@QueryParam("row")String rowKey) throws IOException {
        return sourceLogService.get(rowKey);
    }

}
