package com.engagepoint.report.ws;

import com.engagepoint.report.transform.TransformService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import java.io.IOException;

@Path("/transform")
public class TransformWebService {
    @Autowired
    private TransformService transformService;

    @PUT
    public void transform() throws InterruptedException, IOException, ClassNotFoundException {
        transformService.transform();
    }
}
