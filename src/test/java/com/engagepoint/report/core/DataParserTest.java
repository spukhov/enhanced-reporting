package com.engagepoint.report.core;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.*;

public class DataParserTest {


    @Test
    public void shouldParseXml1() throws Exception {
        InputStream inputStream = DataParserTest.class.getResourceAsStream("/testRequests/base-event.xml");
        EventObjectDto dto = DataParser.parse(inputStream);
        assertNotNull(dto);
        assertEquals("BaseEvent", dto.getName());
        assertNotNull(dto.getTimestamp());
        assertEquals("sourceId", dto.getSourceId());
        assertEquals("172.16.32.98", dto.getSourceAddress());
        assertEquals("329", dto.getInfo());
        assertEquals(0, dto.getAttributes().size());
    }

    @Test
    public void shouldParseXml2() throws Exception {
        InputStream inputStream = DataParserTest.class.getResourceAsStream("/testRequests/service-invocation.xml");
        EventObjectDto dto = DataParser.parse(inputStream);
        assertNotNull(dto);
        assertEquals("ServiceInvocationEvent", dto.getName());
        assertNotNull(dto.getTimestamp());
        assertEquals("sourceID", dto.getSourceId());
        assertEquals("172.16.32.96", dto.getSourceAddress());
        assertEquals("den4egg", dto.getInfo());
        assertEquals(6, dto.getAttributes().size());
        assertEquals("Text", dto.getAttribute("response"));
        assertEquals("1234567", dto.getAttribute("duration"));
        assertEquals("Text", dto.getAttribute("errorResponse"));
        assertEquals("false", dto.getAttribute("failed"));
        assertEquals("Text", dto.getAttribute("destination"));
        assertEquals("Text", dto.getAttribute("request"));
    }
}